package com.example.karaki.kadai1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


/**
 * Created by k.araki on 2017/11/12.
 */

public class MyFragment extends Fragment{
    private int mPage = 0;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstancestate){
        super.onViewCreated(view, savedInstancestate);
        Button button = (Button)view.findViewById(R.id.button);
        Button button2 = (Button)view.findViewById(R.id.button2);
        Button button3 = (Button)view.findViewById(R.id.button3);
        Button button4 = (Button)view.findViewById(R.id.button4);
        Button button5 = (Button)view.findViewById(R.id.button5);
        Button button6 = (Button)view.findViewById(R.id.button6);
        final EditText editText = (EditText)view.findViewById(R.id.editText);
        final PaintView paintView = (PaintView)view.findViewById(R.id.view2);

        switch (mPage){
            case 0:

                private Activity activity = null;
                public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        activity.setContentView(R.layout.flagment0);
                    }

                button.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        String message = "error";
                        try {
                            Context context = null;
                            FileOutputStream fos = context.openFileOutput("text.txt", context.MODE_PRIVATE);
                            OutputStreamWriter writer = new OutputStreamWriter(fos);
                            writer.write(editText.getText().toString());
                            writer.flush();
                            writer.close();
                            Toast.makeText(view.getContext(), "保存が完了しました！", Toast.LENGTH_LONG).show();
                        } catch (FileNotFoundException e) {
                            message = e.getMessage();
                            e.printStackTrace();
                        } catch (IOException e) {
                            message = e.getMessage();
                            e.printStackTrace();
                        }

                    }
                });
                button2.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        editText.setText("");
                    }
                });

                button3.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Context context = null;
                        try {
                            FileInputStream in = context.openFileInput("text.txt");
                            byte[] buff = new byte[1024];
                            int hasRead = 0;

                            StringBuilder sb = new StringBuilder();
                            while ((hasRead = in.read(buff)) > 0) {
                                sb.append(new String(buff, 0, hasRead));
                            }
                            in.close();
                            ;
                            ((EditText) view.findViewById(R.id.editText)).setText(sb.toString());
                            Toast.makeText(view.getContext(), "読み込みが完了しました！", Toast.LENGTH_LONG).show();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });

            case 1:
                button4.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        paintView.clear();
                    }

                });

                button5.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        try{
                            Context context = null;
                            FileInputStream inimage = context.openFileInput("image.png");

                            BitmapFactory.Options imageOptions = new BitmapFactory.Options();
                            imageOptions.inJustDecodeBounds = true;
                            BitmapFactory.decodeStream(inimage, null, imageOptions);
                            Log.v("image", "Original Image Size: " + imageOptions.outWidth + " x " + imageOptions.outHeight);

                            inimage.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        Toast.makeText(view.getContext(), "読込みが完了しました！", Toast.LENGTH_LONG).show();
                    }
                });

                button6.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {

                        String message = "Error";
                        try{

                            FileOutputStream fos = null;
                            fos = new FileOutputStream(new File("image.png"));

                            Bitmap bitmap = null;
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.close();
                            Toast.makeText(view.getContext(), "保存が完了しました！", Toast.LENGTH_LONG).show();
                        }catch (FileNotFoundException e) {
                            message = e.getMessage();
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        }
    }
    public  void setPage(int page){
        mPage = page;
    }
}
