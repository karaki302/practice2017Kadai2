package com.example.karaki.kadai1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

/**
 * Created by k.araki on 2017/11/12.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {
    private static  final int PAGE_NUM = 2;

    public MyPagerAdapter(FragmentManager fm){super(fm);}


    @Override
    public Fragment getItem(int position){
        Fragment fragment = null;
        switch (position){
            case 0: case 1:
                fragment = new MyFragment();
                break;
        }
        ((MyFragment)fragment).setPage(position);
        return fragment;
    }

    public int getCount() {return PAGE_NUM;}
}
